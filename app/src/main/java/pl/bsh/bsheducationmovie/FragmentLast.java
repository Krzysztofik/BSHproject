package pl.bsh.bsheducationmovie;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Szymon on 2017-07-07.
 */

public class FragmentLast extends Fragment {


    Button nextButton;
    ImageView mBackButton = null;
    TextView mQues;
    ImageView mAdminButton = null;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle saveInstanceState) {
        final View inflated = inflater.inflate(R.layout.fragment_last, container, false);
        mQues = (TextView) inflated.findViewById(R.id.question_text);
        mBackButton = (ImageView) inflated.findViewById(R.id.back_button);
        nextButton=(Button)inflated.findViewById(R.id.menu_button);
        nextButton.setOnClickListener(mClick);
        mBackButton.setOnClickListener(mClick);

        mAdminButton=(ImageView)inflated.findViewById(R.id.imagePerson);
        mAdminButton.setOnClickListener(gotoadmin);

        return inflated;

    }

    View.OnClickListener mClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), ActivityMain.class);
            startActivity(intent);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    View.OnClickListener gotoadmin=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ((ActivityMain)getActivity()).GoToAdmin();
        }
    };
}
