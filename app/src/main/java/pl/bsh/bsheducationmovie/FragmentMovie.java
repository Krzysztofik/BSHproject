package pl.bsh.bsheducationmovie;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Locale;

import pl.bsh.bsheducationmovie.Data.AccountInfo;
import pl.bsh.bsheducationmovie.Data.SharedPrefs;

/**
 * Created by Szymon on 2017-07-04.
 */

public class FragmentMovie extends Fragment {

    VideoView mVideo = null;
    ImageView mPlayButton = null;
    ImageView mPauseButton = null;
    ImageView mBackButton = null;
    ImageView mAdminButton = null;
    RelativeLayout mRelativeLayout = null;
    private CompleteMovie mCallback = null;
    private boolean VideoStatus = false;
    private int mPositionWhenPaused = -1;
    private final Handler mHandler = new Handler();
    private AccountInfo mAccountInfo;
    private int counter=0;
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle saveInstanceState) {

        final View inflated = inflater.inflate(R.layout.fragment_movie, container, false);
        mVideo = (VideoView) inflated.findViewById(R.id.video_view);
        mAccountInfo = ((ActivityMain) getActivity()).mAccountInfo;
        if (mAccountInfo.movie == null) {
            if (mAccountInfo.movies.size() == 0) {
                Locale mlocal = getResources().getConfiguration().locale;
                if (mlocal.toString().equals("pl_PL")) {
                    mVideo.setVideoPath("android.resource://" + getActivity().getPackageName() + "/" + R.raw.goscie);
                } else {
                    mVideo.setVideoPath("android.resource://" + getActivity().getPackageName() + "/" + R.raw.visitors);
                }
            } else {
                    mVideo.setVideoPath(mAccountInfo.movies.get(counter++));
            }
        } else {
            mVideo.setVideoPath(mAccountInfo.movie);
        }

        mVideo.setOnTouchListener(mlistenerTouch);
        mVideo.setOnCompletionListener(completionListener);
        if (saveInstanceState != null) {

        }
        mRelativeLayout = (RelativeLayout) inflated.findViewById(R.id.button_view);
        mPlayButton = (ImageView) inflated.findViewById(R.id.play_button);
        mPauseButton = (ImageView) inflated.findViewById(R.id.pause_button);
        mBackButton = (ImageView) inflated.findViewById(R.id.back_button);

        mPlayButton.setOnClickListener(mlistener);
        mPauseButton.setOnClickListener(mlistener);
        mBackButton.setOnClickListener(mListenerBack);
        mAdminButton = (ImageView) inflated.findViewById(R.id.imagePerson);
        mAdminButton.setOnClickListener(gotoadmin);
        return inflated;

    }


    View.OnTouchListener mlistenerTouch = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (mVideo.isPlaying()) {
                mPauseButton.setVisibility(View.VISIBLE);
            } else {
                mPlayButton.setVisibility(View.VISIBLE);
            }
            mBackButton.setVisibility(View.VISIBLE);
            mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.grey));
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mPauseButton.setVisibility(View.GONE);
                    mPlayButton.setVisibility(View.GONE);
                    mBackButton.setVisibility(View.GONE);
                    mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.white_trans));
                }
            }, 2000);
            return false;
        }
    };

    MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            if(mAccountInfo.movie == null&&mAccountInfo.movies.size() == 0){
                mCallback.onChangeFragment();
            }
            else
                {
                    if(counter<mAccountInfo.movies.size())
                    {
                        mVideo.setVideoPath(mAccountInfo.movies.get(counter));
                        mVideo.start();
                        counter++;
                    }
                    else{
                        mCallback.onChangeFragment();
                    }
                }

        }
    };

    View.OnClickListener mlistener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.play_button:
                    mVideo.start();
                    mPlayButton.setVisibility(View.GONE);
                    mBackButton.setVisibility(View.GONE);
                    mRelativeLayout.setBackgroundColor(getResources().getColor(R.color.white_trans));
                    VideoStatus = true;
                    break;
                case R.id.pause_button:
                    mVideo.pause();
                    mPauseButton.setVisibility(View.GONE);
                    mPlayButton.setVisibility(View.VISIBLE);
                    mBackButton.setVisibility(View.VISIBLE);
                    break;
            }

        }
    };


    View.OnClickListener mListenerBack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (VideoStatus) {
                mVideo.pause();
                mPauseButton.setVisibility(View.GONE);
                mPlayButton.setVisibility(View.VISIBLE);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.alert_message)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                        .setNegativeButton(getString(R.string.no), dialogClickListener).show();
            } else {
                Intent intent = new Intent(getActivity(), ActivityMain.class);
                startActivity(intent);
            }
        }
    };

    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    View.OnClickListener gotoadmin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ((ActivityMain) getActivity()).GoToAdmin();
        }
    };

    public interface CompleteMovie {
        void onChangeFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (CompleteMovie) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void onPause() {
        super.onPause();
        mPositionWhenPaused = mVideo.getCurrentPosition();
        mVideo.stopPlayback();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void onResume() {
        super.onResume();
        if (mPositionWhenPaused >= 0) {
            mVideo.seekTo(mPositionWhenPaused);
            mPositionWhenPaused = -1;
            mVideo.start();
        }
    }
}
