package pl.bsh.bsheducationmovie.content;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.bsh.bsheducationmovie.R;

/**
 * Created by Szymon on 2017-08-29.
 */

public class AdminTestAdapter extends RecyclerView.Adapter<ViewHolderBase> {

    public List<String> mData;
    public List<Boolean> mValue;
    public Context mContext;
    MYCustomeEditTextWatcher watcher=new MYCustomeEditTextWatcher();

    public AdminTestAdapter(final List<String> data, final List<Boolean> value, Context context) {
        mData = data == null ? new ArrayList<String>() : data;
        mValue = value == null ? new ArrayList<Boolean>() : value;
        mContext = context;
    }

    @Override
    public ViewHolderBase onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdminTestObjectViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolderBase holder, final int position) {
        final AdminTestObjectViewHolder headerHolder = (AdminTestObjectViewHolder) holder;
        if (mValue.get(position) == null) {
            headerHolder.textView.setText(mContext.getString(R.string.question));
            headerHolder.editText.setText(mData.get(position));
            headerHolder.stateButton.setVisibility(View.GONE);
        } else {
            headerHolder.textView.setText(mContext.getString(R.string.answers));
            headerHolder.editText.setText(mData.get(position));
            headerHolder.stateButton.setText(mValue.get(position).toString());
        }
        watcher.updatePosition(position);
        headerHolder.editText.addTextChangedListener(watcher);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private class MYCustomeEditTextWatcher implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            mData.set(position, charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }
}
