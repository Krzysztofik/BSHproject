package pl.bsh.bsheducationmovie.content;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Szymon on 2017-08-22.
 */

public class ViewHolderBase extends RecyclerView.ViewHolder {

    public ViewHolderBase(final ViewGroup parent, final int layoutRes) {
        super(LayoutInflater.from(parent.getContext()).inflate(layoutRes,parent,false));
    }
}
