package pl.bsh.bsheducationmovie.content;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Szymon on 2017-08-22.
 */

public class TestObjectAdapter extends RecyclerView.Adapter<ViewHolderBase> {


    public List<String> mData;
    private List<Boolean> value;

    public TestObjectAdapter(final List<String> data) {
        mData = data == null ? new ArrayList<String>() : data;
        value = new ArrayList<>();
    }

    @Override
    public ViewHolderBase onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TestObjectViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolderBase holder, final int position) {
        final TestObjectViewHolder headerHolder = (TestObjectViewHolder) holder;
        headerHolder.mText.setText(mData.get(position));
        headerHolder.mCheckBox.setChecked(false);
        headerHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                value.set(position,b);
            }
        });
        value.add(false);
    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public List<Boolean> getValueList()
    {
        return value;
    }

}
