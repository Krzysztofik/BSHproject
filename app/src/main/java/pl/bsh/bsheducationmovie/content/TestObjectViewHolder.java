package pl.bsh.bsheducationmovie.content;

import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import pl.bsh.bsheducationmovie.R;

/**
 * Created by Szymon on 2017-08-22.
 */

public class TestObjectViewHolder extends ViewHolderBase {

    public CheckBox mCheckBox;
    public TextView mText;

    public TestObjectViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_answers);
        mCheckBox =(CheckBox)itemView.findViewById(R.id.radio_button);
        mText=(TextView)itemView.findViewById(R.id.answers);
    }
}
