package pl.bsh.bsheducationmovie.content;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pl.bsh.bsheducationmovie.R;

/**
 * Created by Szymon on 2017-08-29.
 */

public class AdminTestObjectViewHolder extends ViewHolderBase {

    public EditText editText;
    public TextView textView;
    public Button stateButton;


    public AdminTestObjectViewHolder(ViewGroup parent) {
        super(parent, R.layout.item_admin_question);
        editText=(EditText)itemView.findViewById(R.id.edit_text);
        textView=(TextView)itemView.findViewById(R.id.label_text);
        stateButton=(Button)itemView.findViewById(R.id.state_button);
    }
}
