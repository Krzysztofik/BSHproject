package pl.bsh.bsheducationmovie;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import pl.bsh.bsheducationmovie.Data.AccountInfo;
import pl.bsh.bsheducationmovie.Data.SharedPrefs;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Szymon on 2017-08-16.
 */

public class FragmentMovieAdmin extends Fragment {
    ImageView image;
    VideoView videoView;
    RadioButton basebutton;
    RadioButton changeVideButton;
    RadioButton addMovieButton;
    ListView listmovie;
    private AccountInfo mAccountInfo;
    private SharedPrefs prefs;
    ArrayAdapter<String> mAdapter;
    private List<String> list = new ArrayList<>();
    Context mContext;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle saveInstanceState) {

        final View inflated = inflater.inflate(R.layout.fragment_movie_admin, container, false);
        mContext = getActivity();
        mAccountInfo = ((ActivityAdmin) getActivity()).mAccountInfo;
        prefs = ((ActivityAdmin) getActivity()).mPrefs;

        listmovie = (ListView) inflated.findViewById(R.id.list_movie);

        videoView = (VideoView) inflated.findViewById(R.id.actual_video);
        changeVideButton = (RadioButton) inflated.findViewById(R.id.change_button);
        changeVideButton.setOnClickListener(changeListener);
        basebutton = (RadioButton) inflated.findViewById(R.id.base_movie);
        basebutton.setOnClickListener(changeListener);
        addMovieButton = (RadioButton) inflated.findViewById(R.id.add_button);
        addMovieButton.setOnClickListener(changeListener);
        mAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1);


        setButton();
        listmovie.setAdapter(mAdapter);
        listmovie.setOnItemClickListener(selectItem);
        return inflated;
    }


    private void CheckRadioGroup() {
        if (basebutton.isChecked()) {
            Locale mlocal = getResources().getConfiguration().locale;
            if (mlocal.toString().equals("pl_PL")) {
                videoView.setVideoPath("android.resource://" + mContext.getPackageName() + "/" + R.raw.goscie);
            } else {
                videoView.setVideoPath("android.resource://" + mContext.getPackageName() + "/" + R.raw.visitors);
            }
            videoView.start();
        }
        if (changeVideButton.isChecked()) {
            videoView.setVideoPath(mAccountInfo.movie);
            videoView.start();

        }
        if (addMovieButton.isChecked()) {
            listmovie.setVisibility(View.VISIBLE);
            mAdapter.clear();
            for (int x = 0; x < mAccountInfo.movies.size(); x++) {


                if (mAccountInfo.movies.get(x).equals("android.resource://" + mContext.getPackageName() + "/" + R.raw.goscie) ||
                        mAccountInfo.movies.get(x).equals("android.resource://" + mContext.getPackageName() + "/" + R.raw.visitors)) {
                    mAdapter.add(getString(R.string.admin_movie_base_movie));
                } else {
                    mAdapter.add(mAccountInfo.movies.get(x));
                }

            }

        }

    }

    View.OnClickListener changeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.change_button:
                    clear();
                    addMove(R.id.change_button);
                    break;
                case R.id.base_movie:
                    clear();
                    mAccountInfo.button_id = R.id.base_movie;
                    mAccountInfo.save(prefs);
                    mAccountInfo.movie = null;
                    setButton();
                    break;
                case R.id.add_button:
                    listmovie.setVisibility(View.VISIBLE);
                    if (mAccountInfo.movie == null && (mAccountInfo.movies == null || mAccountInfo.movies.size() == 0)) {
                        Locale mlocal = getResources().getConfiguration().locale;
                        if (mlocal.toString().equals("pl_PL")) {
                            mAccountInfo.movies.add("android.resource://" + mContext.getPackageName() + "/" + R.raw.goscie);
                        } else {
                            mAccountInfo.movies.add("android.resource://" + mContext.getPackageName() + "/" + R.raw.visitors);
                        }
                        mAdapter.add(getString(R.string.admin_movie_base_movie));
                    } else {
                        if (mAccountInfo.movie != null) {
                            mAdapter.clear();
                            list.add(mAccountInfo.movie);
                            mAdapter.add(mAccountInfo.movie);
                            mAccountInfo.movies.add(mAccountInfo.movie);
                            mAccountInfo.movie = null;
                        }
                    }
                    addMove(R.id.add_button);


                    break;
            }


        }
    };

    private void clear() {
        mAccountInfo.movies.clear();
        list.clear();
        mAdapter.clear();
        listmovie.setVisibility(View.GONE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        String name;
        if (resultCode == RESULT_OK) {
            Uri selectedMediaUri = data.getData();
            if (selectedMediaUri.toString().contains("video")) {
                videoView.setVideoPath(selectedMediaUri.toString());
                videoView.start();
                name = getFileDetailFromUri(mContext, selectedMediaUri);
                if (!list.contains(name)) {
                    list.add(name);
                    mAdapter.add(name);

                    if (requestCode == 1) {
                        mAccountInfo.button_id = R.id.change_button;
                        mAccountInfo.movie = name;
                        CheckRadioGroup();
                    }
                    if (requestCode == 2) {
                        mAccountInfo.button_id = R.id.add_button;
                        mAccountInfo.movies.add(name);
                    }
                }

                mAccountInfo.save(prefs);

            }
        } else {
            setButton();
        }
    }


    private void setButton() {
        if (mAccountInfo.button_id != 0) {
            switch (mAccountInfo.button_id) {
                case R.id.change_button:
                    changeVideButton.setChecked(true);
                    break;
                case R.id.base_movie:
                    basebutton.setChecked(true);
                    break;
                case R.id.add_button:
                    addMovieButton.setChecked(true);
            }
        } else {
            basebutton.setChecked(true);
        }
        CheckRadioGroup();
    }

    public String getFileDetailFromUri(final Context context, final Uri uri) {
        try {
            String wholeID = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                wholeID = DocumentsContract.getDocumentId(uri);
            }
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Video.Media.DATA};
            String sel = MediaStore.Video.Media._ID + "=?";
            Cursor cursor = getActivity().getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{id}, null);
            String filePath = "";

            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } catch (IllegalArgumentException e) {
            Log.e(e.toString(), getActivity().toString());
            return null;
        }
    }


    private void addMove(int id) {
        try {
            PackageManager m = mContext.getPackageManager();
            String s = getActivity().getPackageName();
            PackageInfo p = null;
            p = m.getPackageInfo(s, 0);
            s = p.applicationInfo.dataDir;
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            Uri uri = Uri.parse(s);
            intent.setDataAndType(uri, "video/*");
            if (id == R.id.change_button) {
                startActivityForResult(Intent.createChooser(intent, "Select Video"), 1);
            } else {
                startActivityForResult(Intent.createChooser(intent, "Select Video"), 2);
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    AdapterView.OnItemClickListener selectItem=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            videoView.setVideoPath(mAccountInfo.movies.get(i));
            videoView.start();
        }
    };
}

