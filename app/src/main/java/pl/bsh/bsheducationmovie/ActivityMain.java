package pl.bsh.bsheducationmovie;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.Locale;

public class ActivityMain extends ActivityBase implements FragmentMovie.CompleteMovie, FragmentTest.OnWrongValue {


    ImageView mStartButton = null;
    LinearLayout mMainView = null;
    ImageView mImageViewPl = null;
    ImageView mImageViewUk = null;
    ImageView mImageAdminButton = null;
    private PopupWindow mAccountPopup;
    private ViewGroup mCoordinator;
    private static final String FRAG_TAG = "ma.tag";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        mCoordinator = (ViewGroup) findViewById(R.id.main_coordinator);
        mStartButton = (ImageView) findViewById(R.id.start_button);
        mStartButton.setOnClickListener(clickListener);
        mMainView = (LinearLayout) findViewById(R.id.main_view);

        mImageViewPl = (ImageView) findViewById(R.id.flags_pl);
        mImageViewPl.setOnClickListener(mListenerLg);
        mImageViewUk = (ImageView) findViewById(R.id.flags_uk);
        mImageViewUk.setOnClickListener(mListenerLg);
        mImageAdminButton = (ImageView) findViewById(R.id.imagePerson);
        mImageAdminButton.setOnClickListener(NewActivity);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        //mAccountInfo = new AccountInfo(mPrefs);
    }

    View.OnClickListener mListenerLg = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Locale locale = null;
            switch (view.getId()) {
                case R.id.flags_pl:
                    locale = new Locale("pl", "PL");
                    break;
                case R.id.flags_uk:
                    locale = new Locale("en", "US");
                    break;
                default:
                    Log.e(view.toString(), "Error value");
                    break;
            }
            if (locale != null) {
                changelocalization(locale);
            }
        }
    };
    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            final FragmentMovie frag = new FragmentMovie();
            getFragmentManager().beginTransaction().add(R.id.fragment_main, frag).commit();
            mMainView.setVisibility(View.GONE);
//            final FragmentTest frag = new FragmentTest();
//            getFragmentManager().beginTransaction().replace(R.id.fragment_main, frag).commit();
//            mMainView.setVisibility(View.GONE);
        }
    };

    private void changelocalization(Locale mlocale) {
        Locale mlocal = getResources().getConfiguration().locale;
        if (!mlocale.toString().toLowerCase().equals(mlocal.toString().toLowerCase())) {
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = mlocale;
            res.updateConfiguration(conf, dm);
            Intent refresh = new Intent(this, ActivityMain.class);
            startActivity(refresh);
        }
    }

    @Override
    public void onChangeFragment() {
        final FragmentTest frag = new FragmentTest();
        getFragmentManager().beginTransaction().replace(R.id.fragment_main, frag).commit();
        mMainView.setVisibility(View.GONE);
    }


    @Override
    public void refreshMovie() {
        final FragmentMovie frag = new FragmentMovie();
        getFragmentManager().beginTransaction().replace(R.id.fragment_main, frag).commit();
        mMainView.setVisibility(View.GONE);
    }

    @Override
    public void FinishView() {
        final FragmentLast frag = new FragmentLast();
        getFragmentManager().beginTransaction().replace(R.id.fragment_main, frag).commit();
        mMainView.setVisibility(View.GONE);
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            //finish();
//            super.onBackPressed();
//        }
//
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, getResources().getText(R.string.back_pres), Toast.LENGTH_SHORT).show();
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 2000);
    }

    View.OnClickListener NewActivity = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            GoToAdmin();
        }
    };

    public void GoToAdmin() {
        final View inflated = getLayoutInflater().inflate(R.layout.popup_account_dissconnected, mCoordinator, false);
        mAccountPopup = new PopupWindow(inflated, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
        mAccountPopup.setFocusable(true);

        final TextView editLogin = (TextView) mAccountPopup.getContentView().findViewById(R.id.popup_account_edit_login);
        final TextView editPass = (TextView) mAccountPopup.getContentView().findViewById(R.id.popup_account_edit_password);

        editPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    View view = v;
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });

        final Button editButton = (Button) mAccountPopup.getContentView().findViewById(R.id.popup_account_action);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int x = 0; x < mAccountInfo.userList.size(); x++) {
                    if (editLogin.getText().toString().equals(mAccountInfo.userList.get(x).id)) {
                        if (editPass.getText().toString().equals(mAccountInfo.userList.get(x).pass)) {
                            mAccountPopup.dismiss();
                            CallToAdminActivity();
                        } else {
                            editLogin.setText("");
                            editPass.setText("");
                        }
                    }
                }
                editLogin.setText("");
                editPass.setText("");

            }
        });
        inflated.setOnTouchListener(new View.OnTouchListener()

        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                mAccountPopup.dismiss();
                return false;
            }
        });
        mAccountPopup.showAtLocation(mCoordinator, Gravity.TOP, 0, 0);
    }

    public void CallToAdminActivity() {
        Intent intent = new Intent(this, ActivityAdmin.class);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Intent intent = new Intent(this, ActivityPermissions.class);
                    startActivity(intent);
                }
                return;
            }

        }
    }
}
