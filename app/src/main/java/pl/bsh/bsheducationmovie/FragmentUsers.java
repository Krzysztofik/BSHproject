package pl.bsh.bsheducationmovie;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import pl.bsh.bsheducationmovie.Data.Account;
import pl.bsh.bsheducationmovie.Data.AccountInfo;
import pl.bsh.bsheducationmovie.Data.SharedPrefs;

/**
 * Created by Szymon on 2017-08-14.
 */

public class FragmentUsers extends Fragment {

    private SharedPrefs mSharedPrefs;
    private AccountInfo mAccount;
    private int position;
    ListView mList;
    ArrayAdapter<String> mAdapter;
    Button addbutton;
    Button removebutton;
    EditText userText;
    EditText passText;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle saveInstanceState) {

        final View inflated = inflater.inflate(R.layout.fragment_users, container, false);

        mSharedPrefs = ((ActivityBase) getActivity()).returnSharePrefs();
        //mAccount = new AccountInfo(mSharedPrefs);
        mAccount=((ActivityBase)getActivity()).mAccountInfo;
        mList = (ListView) inflated.findViewById(R.id.list_user);
        mAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, returnListUser());
        mList.setAdapter(mAdapter);
        mList.setOnItemClickListener(selectItem);
        addbutton = (Button) inflated.findViewById(R.id.admin_button_add);
        addbutton.setOnClickListener(addListener);
        removebutton = (Button) inflated.findViewById(R.id.admin_button_remove);
        removebutton.setOnClickListener(removeListener);

        userText = (EditText) inflated.findViewById(R.id.admin_user_text);
        passText = (EditText) inflated.findViewById(R.id.admin_pass_text);

        return inflated;
    }

    private List<String> returnListUser() {
        List<String> list = new ArrayList<>();

        for (int x = 0; x < mAccount.userList.size(); x++) {
            list.add(mAccount.userList.get(x).id);
        }
        return list;
    }

    View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!userText.getText().equals("") && !userText.getText().equals("")) {
                Account account = new Account();
                account.id = userText.getText().toString();
                account.pass = passText.getText().toString();
                mAccount.userList.add(account);
                mAccount.save(mSharedPrefs);
                mAdapter.add(userText.getText().toString());
            }
        }
    };

    View.OnClickListener removeListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mAccount.userList.size() > 1) {
                mAdapter.remove(mAccount.userList.get(position).id);
                mAccount.clear(mSharedPrefs,mAccount.userList.get(position).id );

            }
        }
    };

    AdapterView.OnItemClickListener selectItem = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            position = i;
        }
    };
}
