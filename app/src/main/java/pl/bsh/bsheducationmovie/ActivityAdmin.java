package pl.bsh.bsheducationmovie;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import pl.bsh.bsheducationmovie.Data.AccountInfo;

public class ActivityAdmin extends ActivityBase {

    ImageView signoutButton;
    ImageView menuButton;
    Button usersButton;
    Button movieButton;
    Button answersButton;
    LinearLayout buttonLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_admin);

        signoutButton = (ImageView) findViewById(R.id.back_button);
        signoutButton.setOnClickListener(backtoMain);
        menuButton = (ImageView) findViewById(R.id.menu_button);
        menuButton.setOnClickListener(menulistner);

        usersButton = (Button) findViewById(R.id.button_user);
        usersButton.setOnClickListener(buttonlistner);
        movieButton=(Button)findViewById(R.id.button_movie);
        movieButton.setOnClickListener(buttonlistner);
        answersButton=(Button)findViewById(R.id.admin_question_button);
        answersButton.setOnClickListener(buttonlistner);
        buttonLayout = (LinearLayout) findViewById(R.id.button_layout);

        final FragmentUsers fragmentUsers = new FragmentUsers();
        getFragmentManager().beginTransaction().replace(R.id.fragment_menu, fragmentUsers).commit();
    }

    View.OnClickListener backtoMain = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            calltoMain();
        }
    };

    private void calltoMain() {
        Intent intent = new Intent(this, ActivityMain.class);
        startActivity(intent);
    }

    View.OnClickListener menulistner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            buttonLayout.setVisibility(getStatusButton(buttonLayout.getVisibility()) ? View.VISIBLE : View.GONE);
        }
    };

    private boolean getStatusButton(int status) {
        if (status != View.VISIBLE) {
            return true;
        } else {
            return false;
        }
    }

    View.OnClickListener buttonlistner = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.button_user:
                    final FragmentUsers fragmentUsers = new FragmentUsers();
                    getFragmentManager().beginTransaction().replace(R.id.fragment_menu, fragmentUsers).commit();
                    break;
                case R.id.button_movie:
                    final FragmentMovieAdmin fragmentMovie = new FragmentMovieAdmin();
                    getFragmentManager().beginTransaction().replace(R.id.fragment_menu, fragmentMovie).commit();
                    break;
                case R.id.admin_question_button:
                    final FragmentTestAdmin fragmentTestAdmin=new FragmentTestAdmin();
                    getFragmentManager().beginTransaction().replace(R.id.fragment_menu,fragmentTestAdmin).commit();
                    break;
            }
        }
    };
}
