package pl.bsh.bsheducationmovie;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pl.bsh.bsheducationmovie.Data.GroupQuestion;
import pl.bsh.bsheducationmovie.content.AdminTestAdapter;

/**
 * Created by Szymon on 2017-08-23.
 */

public class FragmentTestAdmin extends Fragment {

    ImageView mArrowButton;
    LinearLayout mLayoutRadioGroup;
    RadioGroup mRadioGroup;
    TextView mTitle;
    AdminTestAdapter adapter;
    RecyclerView mRecyler;
    Button mSave;
    LinearLayout mLieaLinearLayout;
    TextView mTextView;
    ImageView last;
    ImageView next;
    private int counter = 0;
    private GroupQuestion Question;
    private List<GroupQuestion> mGroupQuestion;
    private List<List<GroupQuestion>> mAllGroup;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle saveInstanceState) {

        final View inflated = inflater.inflate(R.layout.fragment_test_admin, container, false);

        mArrowButton = (ImageView) inflated.findViewById(R.id.arrow_button);
        mArrowButton.setOnClickListener(menuAction);
        mLayoutRadioGroup = (LinearLayout) inflated.findViewById(R.id.layout_radio_group);
        mRadioGroup = (RadioGroup) inflated.findViewById(R.id.radio_group);
        mRadioGroup.setOnCheckedChangeListener(chooseButton);
        mTitle = (TextView) inflated.findViewById(R.id.title);
        mSave = (Button) inflated.findViewById(R.id.save_button);
        mSave.setOnClickListener(SaveQuestion);
        last = (ImageView) inflated.findViewById(R.id.last);
        last.setOnClickListener(lastClick);
        next = (ImageView) inflated.findViewById(R.id.next);
        next.setOnClickListener(nextClick);
        mRecyler = (RecyclerView) inflated.findViewById(R.id.group_recycler);
        mTextView = (TextView) inflated.findViewById(R.id.how_much);
        mLieaLinearLayout = (LinearLayout) inflated.findViewById(R.id.question_group);
        adapter = new AdminTestAdapter(null, null, getActivity());


        mRecyler.setItemAnimator(new DefaultItemAnimator());
        mRecyler.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mRecyler.setAdapter(adapter);

        return inflated;
    }


    View.OnClickListener menuAction = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mArrowButton.getRotation() == 90) {
                mArrowButton.setRotation(270);
                mLayoutRadioGroup.setVisibility(View.GONE);
            } else {
                mArrowButton.setRotation(90);
                mLayoutRadioGroup.setVisibility(View.VISIBLE);
            }
        }
    };

    RadioGroup.OnCheckedChangeListener chooseButton = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
            RadioButton radio = (RadioButton) radioGroup.findViewById(i);
            mTitle.setText(radio.getText());
            if (radio.getText().equals(getString(R.string.edition_test))) {
                parserQuestion();
                parserQuestionToList();
                mTextView.setText(getString(R.string.number_question_group, counter+1, mAllGroup.size()));
                mLieaLinearLayout.setVisibility(View.VISIBLE);
            } else {
                clearDataset();
                mLieaLinearLayout.setVisibility(View.GONE);
            }
        }
    };

    protected void clearDataset() {
        final int prevElements = adapter.mData.size();
        final int prevElement = adapter.mValue.size();
        adapter.mData.clear();
        if (prevElements > 0) {
            adapter.notifyItemRangeRemoved(0, prevElements);
        }
        adapter.mValue.clear();
        if (prevElement > 0) {
            adapter.notifyItemRangeRemoved(0, prevElement);
        }


    }

    private void parserQuestion() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.question)));
            String line = reader.readLine();
            mGroupQuestion = new ArrayList<>();
            mAllGroup = new ArrayList<>();
            while (line != null) {
                if (line.equals("/") || line.equals("//")) {
                    Question = new GroupQuestion();
                    line = reader.readLine();
                    if (line != null) {
                        Question.question = line;
                    }
                    line = reader.readLine();
                    if (line != null) {
                        while (line != null && !line.equals("/") && !line.equals("//")) {
                            Question.answers.add(line);
                            Question.valueAnswers.add(Boolean.valueOf(reader.readLine()));
                            line = reader.readLine();
                        }
                        mGroupQuestion.add(Question);
                        if (line.equals("//")) {
                            mAllGroup.add(mGroupQuestion);
                            mGroupQuestion = new ArrayList<>();
                        }
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void parserQuestionToList() {


        for (int y = 0; y < mAllGroup.get(counter).size(); y++) {
            adapter.mData.add(mAllGroup.get(counter).get(y).question);
            adapter.mValue.add(null);
            for (int z = 0; z < mAllGroup.get(counter).get(y).answers.size(); z++) {
                adapter.mData.add(mAllGroup.get(counter).get(y).answers.get(z));
                adapter.mValue.add(mAllGroup.get(counter).get(y).valueAnswers.get(z));
            }
        }

        adapter.notifyItemInserted(0);
    }

    View.OnClickListener SaveQuestion = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };
    View.OnClickListener nextClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (counter < mAllGroup.size()-1) {
                clearDataset();
                counter++;
                parserQuestionToList();
                mTextView.setText(getString(R.string.number_question_group, counter+1, mAllGroup.size()));
            }
        }
    };
    View.OnClickListener lastClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (counter > 0) {
                clearDataset();
                counter--;
                parserQuestionToList();
                mTextView.setText(getString(R.string.number_question_group, counter+1, mAllGroup.size()));
            }
        }
    };
}
