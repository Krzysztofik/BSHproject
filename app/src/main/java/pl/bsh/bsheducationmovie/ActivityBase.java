package pl.bsh.bsheducationmovie;

import android.app.Activity;
import android.os.Bundle;

import pl.bsh.bsheducationmovie.Data.AccountInfo;
import pl.bsh.bsheducationmovie.Data.SharedPrefs;

/**
 * Created by Szymon on 2017-08-16.
 */

public class ActivityBase extends Activity {

    protected SharedPrefs mPrefs;
    protected AccountInfo mAccountInfo;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPrefs = new SharedPrefs(this);
        mAccountInfo = new AccountInfo(mPrefs);
    }

    public SharedPrefs returnSharePrefs() {
        return mPrefs;
    }


}

