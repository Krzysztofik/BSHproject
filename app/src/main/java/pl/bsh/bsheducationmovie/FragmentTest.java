package pl.bsh.bsheducationmovie;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.bsh.bsheducationmovie.Data.GroupQuestion;
import pl.bsh.bsheducationmovie.content.TestObjectAdapter;

/**
 * Created by Szymon on 2017-07-05.
 */

public class FragmentTest extends Fragment {

    TextView mQues;
    Button nextButton;
    ImageView mBackButton = null;
    ImageView mAdminButton = null;
    private int counter = 0;
    private OnWrongValue mCallback;
    TestObjectAdapter adapter;
    RecyclerView recyclerView;
    private GroupQuestion Question;
    private List<GroupQuestion> mGroupQuestion;
    private List<List<GroupQuestion>> mAllGroup;
    Integer group = null;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle saveInstanceState) {

        final View inflated = inflater.inflate(R.layout.fragment_test, container, false);
        mQues = (TextView) inflated.findViewById(R.id.question_text);
        nextButton = (Button) inflated.findViewById(R.id.button_next);
        nextButton.setText(getResources().getText(R.string.next));
        nextButton.setOnClickListener(click);
        mBackButton = (ImageView) inflated.findViewById(R.id.back_button);
        mBackButton.setOnClickListener(mListenerBack);
        mAdminButton = (ImageView) inflated.findViewById(R.id.imagePerson);
        mAdminButton.setOnClickListener(gotoadmin);
        alertshow();
        readFileQuestion();


        recyclerView = (RecyclerView) inflated.findViewById(R.id.page_answers_recyler);

        setFirstQuestion();

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        return inflated;

    }

    private void setFirstQuestion() {
        if (group == null) {
            Random r = new Random();
            group = r.nextInt(mAllGroup.size());
        }
        if (mAllGroup.get(group).size() > counter) {
            adapter = new TestObjectAdapter(mAllGroup.get(group).get(counter).answers);
            mQues.setText(mAllGroup.get(group).get(counter).question);
        }

    }

    private void readFileQuestion() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.question)));
            String line = reader.readLine();
            mGroupQuestion = new ArrayList<>();
            mAllGroup = new ArrayList<>();
            while (line != null) {
                if (line.equals("/") || line.equals("//")) {
                    Question = new GroupQuestion();
                    line = reader.readLine();
                    if (line != null) {
                        Question.question = line;
                    }
                    line = reader.readLine();
                    if (line != null) {
                        while (line != null && !line.equals("/") && !line.equals("//")) {
                            Question.answers.add(line);
                            Question.valueAnswers.add(Boolean.valueOf(reader.readLine()));
                            line = reader.readLine();
                        }
                        mGroupQuestion.add(Question);
                        if (line.equals("//")) {
                            mAllGroup.add(mGroupQuestion);
                            mGroupQuestion = new ArrayList<>();
                        }
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void alertshow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.alert_continue_message)).setPositiveButton(getString(R.string.cont), null).show();
    }

    private void alerterrorshow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.error_alert)).setPositiveButton(getString(R.string.yes), dialogErrorClickListener)
                .setNegativeButton(getString(R.string.no), dialogErrorClickListener).show();
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (mAllGroup.get(group).size() - 1 > counter) {
                if (mAllGroup.get(group).get(counter).valueAnswers.containsAll(adapter.getValueList())) {
                    counter++;
                    clearDataset();
                    mQues.setText(mAllGroup.get(group).get(counter).question);
                    adapter.mData = mAllGroup.get(group).get(counter).answers;
                    adapter.notifyItemInserted(0);
                } else {
                    alerterrorshow();
                }
            } else {
                mCallback.FinishView();
            }
        }

    };

    protected void clearDataset() {
        final int prevElements = adapter.mData.size();

        adapter.mData.clear();
        if (prevElements > 0) {
            adapter.notifyItemRangeRemoved(0, prevElements);
        }
    }

    public interface OnWrongValue {
        void refreshMovie();

        void FinishView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnWrongValue) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    View.OnClickListener mListenerBack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.alert_message)).setPositiveButton(getString(R.string.yes), dialogClickListener)
                        .setNegativeButton(getString(R.string.no), dialogClickListener).show();
            }
        }
    };


    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
                    break;


                case DialogInterface.BUTTON_NEGATIVE:

                    break;
            }
        }
    };

    DialogInterface.OnClickListener dialogErrorClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    mCallback.refreshMovie();
                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    Intent intent = new Intent(getActivity(), ActivityMain.class);
                    startActivity(intent);
                    break;

            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    View.OnClickListener gotoadmin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ((ActivityMain) getActivity()).GoToAdmin();
        }
    };

}
