package pl.bsh.bsheducationmovie.Data;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Szymon on 2017-07-25.
 */

public class AccountInfo {

    public List<Account> userList;
    private String users;
    private String pass;
    public List<String> movies;
    public int button_id;
    public String movie;
    private String mMovies="";

    public AccountInfo() {
    }

    public AccountInfo(final SharedPrefs prefs) {
        userList = parseDataToArray(prefs.Load(SharedPrefs.KEY_USER_LOGIN, null), prefs.Load(SharedPrefs.KEY_USER_PASS, null));
        if (userList == null) {
            Account account = new Account();
            userList = new ArrayList<>();
            account.id = "admin";
            account.pass = "admin";
            userList.add(account);
        }
        movies = parserMovies(prefs.Load(SharedPrefs.KEY_MOVIES, null));
        if(movies==null)
        {
            movies=new ArrayList<>();
        }
        movie = prefs.Load(SharedPrefs.KEY_MOVIE, null);
        button_id = prefs.Load(SharedPrefs.KEY_ADMIN_MOVIE, 0);
    }

    public void save(final SharedPrefs prefs) {
        if (userList != null) {
            parseDataToString();
            prefs.Save(SharedPrefs.KEY_USER_LOGIN, users);
            prefs.Save(SharedPrefs.KEY_USER_PASS, pass);
        }
        if(movies!=null)
        {
            parserMoviesToString();
            prefs.Save(SharedPrefs.KEY_MOVIES,mMovies);
        }
        prefs.Save(SharedPrefs.KEY_MOVIE, movie);
        prefs.Save(SharedPrefs.KEY_ADMIN_MOVIE, button_id);
    }

    public void clear(final SharedPrefs prefs, String user) {

        for (int x = 0; x < userList.size(); x++) {
            if (userList.get(x).id.equals(user)) {
                userList.remove(x);
                save(prefs);
                return;
            }
        }
    }


    private List<Account> parseDataToArray(String users, String pass) {
        if (users != null && pass != null) {
            List<Account> Listaccount = new ArrayList<>();
            char[] Charusers = users.toCharArray();
            char[] Charpass = pass.toCharArray();
            int y = 0;
            Account account = null;
            for (int x = 0; x < Charusers.length; x++) {
                if (account == null) {
                    account = new Account();
                }

                if (Charusers[x] != '@') {
                    account.id += Charusers[x];
                } else {
                    for (; y < Charpass.length; y++) {
                        if (Charpass[y] != '@') {
                            account.pass += Charpass[y];
                        } else {
                            y++;
                            break;
                        }
                    }
                    Listaccount.add(account);
                    account = null;
                }
            }
            return Listaccount;
        } else {
            return null;
        }
    }

    private void parseDataToString() {
        if (userList != null) {
            users = "";
            pass = "";
            for (int x = 0; x < userList.size(); x++) {
                users += userList.get(x).id + "@";
                pass += userList.get(x).pass + "@";
            }

        }
    }

    private void parserMoviesToString()
    {
        if(movies!=null)
        {
            mMovies="";
            for(int x=0;x<movies.size();x++){
                mMovies+=movies.get(x)+"@";
            }
        }
    }

    private List<String> parserMovies(String movie) {
        if (movie != null) {
        List<String> list=new ArrayList<>();
            char[] listmovie=movie.toCharArray();
            String mov="";
            for(int x=0;x<listmovie.length;x++)
            {
                if(listmovie[x]!='@')
                {
                    mov+=listmovie[x];
                }
                else{
                    list.add(mov);
                    mov="";
                }
            }
            return list;
        }
        return null;

    }
}
