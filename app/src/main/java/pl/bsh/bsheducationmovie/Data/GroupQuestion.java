package pl.bsh.bsheducationmovie.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Szymon on 2017-08-23.
 */

public class GroupQuestion {

    public List<String> answers=new ArrayList<>();
    public List<Boolean> valueAnswers=new ArrayList<>();
    public String question;
}
